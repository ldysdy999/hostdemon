/*
配置文件的加载
*/
package conf

import (
	"fmt"

	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

//从环境变量中加载
func LoadFromEnv() error {
	cfg := NewDefaultConfig()
	err := env.Parse(cfg)
	if err != nil {
		return fmt.Errorf("通过环境变量加载配置失败%s", err)
	}
	SetGlobalConfig(cfg)
	return nil
}

//从配置文件总加载
func LoadFromFile(path string) error {
	cfg := NewDefaultConfig()
	_, err := toml.DecodeFile(path, cfg)
	if err != nil {
		return fmt.Errorf("通过配置文件加载配置失败%s", err)
	}
	SetGlobalConfig(cfg)
	return nil
}
