package impl

import (
	"database/sql"

	"gitee.com/ldysdy999/hostdemon/conf"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

var Service *impl = &impl{}

type impl struct {
	log logger.Logger //进行日志打印

	db *sql.DB //数据库，从配置文件总读取配置或环境变量
}

//初始化，如各种依赖
func (i *impl) Init() error {
	i.log = zap.L().Named("Host")
	db, err := conf.C().MySql.GetDB()
	if err != nil {
		return err
	}
	i.db = db
	return nil
}
