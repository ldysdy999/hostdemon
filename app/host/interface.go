package host

import "context"

type Service interface {
	//录入主机信息
	CreateHost(context.Context, *Host) (*Host, error)
	//查询主机列表
	QueryHost(context.Context, *QueryHostRequest) (*Set, error)
	//主机详情
	DescribeHost(context.Context, *DescribeHostRequest) (*Host, error)
	//主机信息修改
	UpdateHost(context.Context, *UpdateHostRequest) (*Host, error)
	//删除主机，到时候会全局广播该事件
	DeleteHost(context.Context, *DeleteHostRequest) (*Host, error)
}

type QueryHostRequest struct {
	PageSize int
	PageNum  int
}

func (req *QueryHostRequest) Offset() int {
	return (req.PageNum - 1) * req.PageSize
}

type DescribeHostRequest struct {
	Id string
}

type UpdateHostRequest struct {
	Id string
}

type DeleteHostRequest struct {
	Id string
}
