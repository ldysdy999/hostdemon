package http

import (
	"gitee.com/ldysdy999/hostdemon/app"
	"gitee.com/ldysdy999/hostdemon/app/host"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
	"github.com/julienschmidt/httprouter"
)

var Api = handler{}

type handler struct {
	host host.Service
	log  logger.Logger
}

//初始化http handler需要依赖Host Service的实例对象
//通过ioc，解耦依赖
func (h *handler) Init() {
	if app.Host == nil {
		panic("dependence service is nil")
	}
	h.host = app.Host
	h.log = zap.L().Named("HOST API")
}

//注册路由
func (h *handler) Registry(r *httprouter.Router) {
	r.POST("/hosts", h.CreateHost)
	r.GET("/hosts", h.QueryHost)
}
